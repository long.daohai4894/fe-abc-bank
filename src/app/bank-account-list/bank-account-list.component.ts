import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BankAccountDataSource} from '../services/bank_accounts.datasource';
import {BankAccountsService} from '../services/bank_accounts.service';
import {MatPaginator} from '@angular/material/paginator';
import {fromEvent, merge} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'bank-account-list',
  templateUrl: './bank-account-list.component.html',
  styleUrls: ['./bank-account-list.component.css']
})
export class BankAccountListComponent implements OnInit, AfterViewInit {

  searchForm: FormGroup;

  dataSource: BankAccountDataSource;

  role: string;

  total: number;

  displayedColumns = [
    'account_number',
    'balance',
    'name',
    'age',
    'gender',
    'email',
    'city',
    'handle_detail'
  ];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private formBuilder: FormBuilder, private bankAccountsService: BankAccountsService, private authService: AuthService) {
    this.role = this.authService.getRole();
  }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      account_number: '',
      email: '',
      city: '',
      gender: '',
      other_fields: '',
    });

    this.dataSource = new BankAccountDataSource(this.bankAccountsService);
    this.dataSource.loadBankAccounts(1, 10, this.searchForm.value);
  }

  ngAfterViewInit() {
    merge(this.paginator.page)
      .pipe(
        tap(() => this.loadBankAccountPage())
      )
      .subscribe();
  }

  searchBankAccount() {
    console.log(this.searchForm.value);
    this.loadBankAccountPage();
  }

  loadBankAccountPage() {
    this.dataSource.loadBankAccounts(
      (this.paginator.pageIndex + 1),
      this.paginator.pageSize,
      this.searchForm.value
    );
  }

}
