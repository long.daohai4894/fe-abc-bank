import {Component, OnInit} from '@angular/core';
import {BankAccountsService} from '../services/bank_accounts.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'bank-account-create',
  templateUrl: './bank-account-create.component.html',
  styleUrls: ['./bank-account-create.component.css']
})
export class BankAccountCreateComponent implements OnInit {

  bank_account_form: FormGroup;

  role: string;

  constructor(
    private bankAccountService: BankAccountsService,
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this.bank_account_form = formBuilder.group({
      account_number: [''],
      balance: 0,
      firstname: [''],
      lastname: [''],
      age: 20,
      gender: [''],
      address: [''],
      employer: [''],
      email: [''],
      city: [''],
      state: [''],
    });
    this.role = this.authService.getRole();
  }

  ngOnInit(): void {
  }

  openSnackBar() {
    this.snackBar.open('Tạo mới bank account thành công', '', {
      duration: 2000,
    });
  }

  createBankAccount() {
    if (this.bank_account_form.valid) {
      this.bankAccountService.createBankAccount(this.bank_account_form.value)
        .subscribe(success => {
          if (success) {
            this.openSnackBar();
            this.router.navigate(['/']);
          }
        });
    }
  }
}
