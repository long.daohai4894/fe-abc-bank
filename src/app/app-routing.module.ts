import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthComponent} from './auth/auth.component';
import {AuthGuard} from './guards/auth.guards';
import {RandomGuard} from './guards/random.guards';
import {BankAccountListComponent} from './bank-account-list/bank-account-list.component';
import {BankAccountDetailComponent} from './bank-account-detail/bank-account-detail.component';
import { BankAccountCreateComponent } from './bank-account-create/bank-account-create.component';
import {BankAccountEditComponent} from './bank-account-edit/bank-account-edit.component';
import { AdminGuards } from './guards/admin.guards';

const routes: Routes = [
  {
    path: 'login',
    component: AuthComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: BankAccountListComponent,
    canActivate: [RandomGuard],
    canLoad: [RandomGuard]
  },
  {
    path: 'bank-account/create',
    component: BankAccountCreateComponent,
    canActivate: [RandomGuard, AdminGuards],
    canLoad: [RandomGuard]
  },
  {
    path: 'bank-account/:id',
    component: BankAccountDetailComponent,
    canActivate: [RandomGuard],
    canLoad: [RandomGuard]
  },
  {
    path: 'bank-account/:id/edit',
    component: BankAccountEditComponent,
    canActivate: [RandomGuard, AdminGuards],
    canLoad: [RandomGuard]
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
