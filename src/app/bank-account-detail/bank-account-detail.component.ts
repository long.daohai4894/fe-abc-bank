import {Component, OnInit} from '@angular/core';
import {BankAccountsService} from '../services/bank_accounts.service';
import {BankAccount} from '../model/bank_account';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'bank-account-detail',
  templateUrl: './bank-account-detail.component.html',
  styleUrls: ['./bank-account-detail.component.css']
})
export class BankAccountDetailComponent implements OnInit {
  bank_account: BankAccount;
  private bankAccountsSubject = new BehaviorSubject<BankAccount>(undefined);

  constructor(private bankAccountService: BankAccountsService, private route: ActivatedRoute) {
    this.bankAccountService.findBankAccountById(this.route.snapshot.params['id']).subscribe(res => {
      this.bank_account = res;
    });
  }

  ngOnInit(): void {
  }

}
