import {Component, OnInit} from '@angular/core';
import {BankAccount} from '../model/bank_account';
import {BankAccountsService} from '../services/bank_accounts.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'bank-account-edit',
  templateUrl: './bank-account-edit.component.html',
  styleUrls: ['./bank-account-edit.component.scss']
})
export class BankAccountEditComponent implements OnInit {

  bank_account: BankAccount;

  bank_account_form: FormGroup;

  constructor(
    private bankAccountService: BankAccountsService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.bankAccountService.findBankAccountById(this.route.snapshot.params['id']).subscribe(res => {
      this.bank_account = res;
      this.bank_account_form = this.formBuilder.group({
        account_number: this.bank_account.account_number,
        balance: this.bank_account.balance,
        firstname: this.bank_account.firstname,
        lastname: this.bank_account.lastname,
        age: this.bank_account.age,
        gender: this.bank_account.gender,
        address: this.bank_account.address,
        employer: this.bank_account.employer,
        email: this.bank_account.email,
        city: this.bank_account.city,
        state: this.bank_account.state,
      });
    });
  }

  openSnackBar() {
    this.snackBar.open('Chỉnh sửa bank account thành công', '', {
      duration: 2000,
    });
  }

  updateBankAccount() {
    if (this.bank_account_form.valid) {
      this.bankAccountService.updateBankAccount(this.route.snapshot.params['id'], this.bank_account_form.value)
        .subscribe(success => {
          if (success) {
            this.openSnackBar();
            this.router.navigate(['/']);
          }
        });
    }
  }

}
