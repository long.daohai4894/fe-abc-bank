import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatToolbarModule} from '@angular/material/toolbar';
import {CoursesService} from './services/courses.service';
import {HttpClientModule} from '@angular/common/http';
import {CourseResolver} from './services/course.resolver';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {AuthComponent} from './auth/auth.component';
import {DefaultLayoutComponent} from './default-layout/default-layout.component';
import {HeaderComponent} from './header/header.component';
import {BankAccountListComponent} from './bank-account-list/bank-account-list.component';
import {BankAccountsService} from './services/bank_accounts.service';
import {BankAccountDetailComponent} from './bank-account-detail/bank-account-detail.component';
import {BankAccountCreateComponent} from './bank-account-create/bank-account-create.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {BankAccountEditComponent} from './bank-account-edit/bank-account-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    DefaultLayoutComponent,
    HeaderComponent,
    BankAccountListComponent,
    BankAccountDetailComponent,
    BankAccountCreateComponent,
    BankAccountEditComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    AppRoutingModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    ReactiveFormsModule
  ],
  providers: [
    BankAccountsService,
    CoursesService,
    CourseResolver
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
