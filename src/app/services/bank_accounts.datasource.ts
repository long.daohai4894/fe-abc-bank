import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BankAccount} from '../model/bank_account';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {BankAccountsService} from './bank_accounts.service';
import {catchError, finalize} from 'rxjs/operators';

export class BankAccountDataSource implements DataSource<BankAccount> {
  private bankAccountsSubject = new BehaviorSubject<BankAccount[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  public total = new BehaviorSubject<number>(0);

  constructor(private bankAccountsService: BankAccountsService) {

  }

  loadBankAccounts(
    pageIndex: number,
    pageSize: number,
    searchForm: object) {
    this.loadingSubject.next(true);
    this.bankAccountsService.findBankAccounts(pageIndex, pageSize, searchForm).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(res => {
      this.bankAccountsSubject.next(res.data);
      this.total.next(res.total);
    }
    );
  }

  connect(collectionViewer: CollectionViewer): Observable<BankAccount[]> {
    return this.bankAccountsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.bankAccountsSubject.complete();
    this.loadingSubject.complete();
  }
}
