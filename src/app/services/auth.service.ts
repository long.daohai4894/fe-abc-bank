import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {of, Observable, BehaviorSubject} from 'rxjs';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { config } from '../config';
import { Tokens } from '../model/tokens';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private readonly ROLE = 'ROLE';
  private readonly USER = 'USER';
  private loggedUser: string;
  private loggedIn = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {}

  openSnackBar(message) {
    console.log(message);
    if (message !== '' && message) {
      this.snackBar.open(message, '', {
        duration: 2000,
      });
    }
  }

  login(user: { email: string, password: string }): Observable<boolean> {
    return this.http.post<any>(`${config.apiUrl}/user/login`, user)
      .pipe(
        tap(tokens => this.doLoginUser(user.email, tokens)),
        mapTo(true),
        catchError(error => {
          this.openSnackBar(error.error.message);
          return of(false);
        }));
  }

  logout() {
    return this.doLogoutUser();
  }

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  isLoggedInHeader() {
    return this.loggedIn.asObservable();
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  getRole() {
    return localStorage.getItem(this.ROLE);
  }

  getLoggedUser() {
    return localStorage.getItem(this.USER);
  }

  private doLoginUser(username: string, tokens: Tokens) {
    this.loggedUser = username;
    this.loggedIn.next(true);
    this.storeTokens(tokens['data']);
  }

  private doLogoutUser() {
    this.loggedUser = null;
    this.loggedIn.next(false);
    this.removeTokens();
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private storeTokens(tokens: Tokens) {
    localStorage.setItem(this.JWT_TOKEN, tokens.token);
    localStorage.setItem(this.ROLE, tokens.role);
    localStorage.setItem(this.USER, tokens.email);
  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem(this.ROLE);
    localStorage.removeItem(this.USER);
  }
}
