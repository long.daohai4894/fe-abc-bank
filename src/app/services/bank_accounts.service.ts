import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, mapTo} from 'rxjs/operators';
import {BankAccount} from '../model/bank_account';
import {config} from '../config';
import {AuthService} from './auth.service';

@Injectable()
export class BankAccountsService {
  private jwt_token: string;

  constructor(private http: HttpClient, private authService: AuthService) {
    this.jwt_token = this.authService.getJwtToken();
  }

  findBankAccountById(bankAccountId: string): Observable<BankAccount> {
    return this.http.get<BankAccount>(`${config.apiUrl}/bank-account/${bankAccountId}`, {
      headers: new HttpHeaders()
        .set('accept', 'application/json')
        .set('Authorization', `JWT ${this.jwt_token}`)
    }).pipe(map(res => res['data']));
  }

  createBankAccount(bankAccount: {
    account_number: string,
    balance: number,
    firstname: string,
    lastname: string,
    age: number,
    gender: string,
    address: string,
    employer: string,
    email: string,
    city: string,
    state: string,
  }): Observable<boolean> {
    return this.http.post<any>(`${config.apiUrl}/bank-account`, bankAccount, {
      headers: new HttpHeaders()
        .set('accept', 'application/json')
        .set('Authorization', `JWT ${this.jwt_token}`)
    }).pipe(
      mapTo(true),
      catchError(error => {
        console.log(error.error);
        return of(false);
      })
    );
  }

  updateBankAccount(bankAccountId: string, bankAccount: {
    account_number: string,
    balance: number,
    firstname: string,
    lastname: string,
    age: number,
    gender: string,
    address: string,
    employer: string,
    email: string,
    city: string,
    state: string,
  }): Observable<boolean> {
    return this.http.put<any>(`${config.apiUrl}/bank-account/${bankAccountId}`, bankAccount, {
      headers: new HttpHeaders()
        .set('accept', 'application/json')
        .set('Authorization', `JWT ${this.jwt_token}`)
    }).pipe(
      mapTo(true),
      catchError(error => {
        console.log(error.error);
        return of(false);
      })
    );
  }

  findBankAccounts(pageNumber = 1, pageSize = 10, searchForm: object): any {
    let listParams = new HttpParams().set('page', pageNumber.toString()).set('pageSize', pageSize.toString());
    if (searchForm['account_number'] !== '') {
      listParams = listParams.append('account_number', searchForm['account_number'].toString());
    }
    if (searchForm['email'] !== '') {
      listParams = listParams.append('email', searchForm['email'].toString());
    }
    if (searchForm['city'] !== '') {
      listParams = listParams.append('city', searchForm['city'].toString());
    }
    if (searchForm['gender'] !== '') {
      listParams = listParams.append('gender', searchForm['gender'].toString());
    }
    if (searchForm['other_fields'] !== '') {
      listParams = listParams.append('all_fields', searchForm['other_fields'].toString());
    }
    return this.http.get(`${config.apiUrl}/bank-account`, {
      headers: new HttpHeaders()
        .set('accept', 'application/json')
        .set('Authorization', `JWT ${this.jwt_token}`),
      params: listParams
    }).pipe(
      map(res => {
      return {data: res['data'], total: res['metadata'].total_items};
    })
  )
    ;
  }
}
