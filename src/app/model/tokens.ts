export class Tokens {
  token: string;
  refreshToken: string;
  role: string;
  email: string;
}
